module github.com/hashicorp/terraform-plugin-log

go 1.16

require (
	github.com/google/go-cmp v0.5.6
	github.com/hashicorp/go-hclog v0.16.1
	github.com/mitchellh/go-testing-interface v1.14.1
	github.com/stretchr/testify v1.3.0 // indirect
)
